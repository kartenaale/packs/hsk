# HSK (Hanyu Shuiping Kaoshi)
Vocabulary to prepare for HSK exams.

The content in the cards has been provided by _Konfuzius Institut Leipzig_.
Automatic translations to traditional characters have been added on top (and
may not be reliable).

Vocabulary is available in _English_ for all HSK levels. There is a separate
pack for _German_, which covers HSK1–HSK4.

## Importing the pack to your phone or PC
Open
[Deployments | Releases](https://gitlab.phaidra.org/kartenaale/packs/hsk/-/releases)
on the left, then click _Browse all APKGS…_ on the newest release. There, click
an APKG, download it, and import into Anki.

If you are completely new to Anki and feel a bit lost, check out the user guide
in
[English](https://gitlab.phaidra.org/kartenaale/getting-started-with-anki/-/blob/main/GUIDE.md)
or
[German](https://gitlab.phaidra.org/kartenaale/getting-started-with-anki/-/blob/main/ANLEITUNG.md).

## Contributing
To add new content to this pack, either add `.apkg` files in the root next to
`content` to include them unchanged, OR add subfolders to `content/` with data
in the form of CSV, images or APKGs and an `.apkg-spec.yaml` file that
configures which HTML to use for the cards and how your data is laid out.

You can use the content in the starter template as your basis or just have a
look to see how it works:
* [content/Facts-from-CSV](https://gitlab.phaidra.org/kartenaale/templates/anki-pack/-/tree/main/content/Facts-from-CSV) for Q/A cards imported from a CSV file,
* [content/Bijection-from-CSV](https://gitlab.phaidra.org/kartenaale/templates/anki-pack/-/tree/main/content/Bijection-from-CSV) for cards that work both ways, where the answer is also a question,
* [content/Bijection-from-Images](https://gitlab.phaidra.org/kartenaale/templates/anki-pack/-/tree/main/content/Bijection-from-Images) for two cards, one to remember the image and one to remember the label in the filename,
* [content/Vocabulary-from-Anki](https://gitlab.phaidra.org/kartenaale/templates/anki-pack/-/tree/main/content/Vocabulary-from-Anki) for vocabulary cards, with an Anki text export of simple notes as the data source.

To get your changes back into the repository and test them, create a new branch
with your changes and open a merge request. Check the APKGs in the CI/CD build
job on the MR and if you are happy, merge to main.

### Releasing a new version
Bump the version number in `package.json` in your merge request.

When your changes land on the main branch, a new release will be created in the
releases section of the project and the kartenaale whatsapp group will be
notified (if you go to the CI/CD job and link with your Whatsapp to send out
the notification - restart the job if necessary).
